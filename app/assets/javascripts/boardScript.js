$(document).ready(function(){

	if(document.getElementById('mainTable') != null) {
		setTableHeight();
	}

    $("#generate_btn").click(function(){
    	location.reload();
    });

    if($("messages").empty()) {
    	console.log("No message");
    }

});


function setTableHeight() {
	document.getElementById('mainTable').style.height = $(document).outerHeight(true) - $("#mainTable").position().top - $("#headerDiv").outerHeight(true) + $("#headerDiv").position().top + "px";  
}
