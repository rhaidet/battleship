$(document).bind "board_index.load", (e,obj) ->

  App.game = App.cable.subscriptions.create "GameChannel",

    connected: ->
      # Called when the subscription is ready for use on the server
      @printMessage("Waiting for opponent...")

    disconnected: ->
      # Called when the subscription has been terminated by the server

    received: (data) ->
      # Called when there is incoming data on the websocket for this channel
      switch data.action
        when "game_start"
          # debugger
          map = data.map
          console.log(map);
          i = 0
          while i < map.length
            cube = map[i]
            j = 0
            while j < cube.length
              # console.log('cube[' + i + '][' + j + '] = ' + cube[j])
              # console.log(coordinatesToId(i, j))
              if(map[i][j] !=0)
                $('#' + coordinatesToId(i, j), $('#myBoard')).text(map[i][j])
              j++
            i++
          $('#opponentBoard tr:not(:first-child').on 'click', 'td:not(:first-child)', ->
            $('#valueLabel').append @id
            App.game.callFireShot(@id)
            return
          $("#messages").empty()
          @printMessage("Game started! You play as #{data.msg}.")
          if data.turn  
            @printMessage("It's your turn!")
          else
            $('#opponentBoard').css 'pointerEvents', 'none'
            @printMessage("It's your opponent's turn!")


        when "make_move"
          [source, target] = data.msg.split("-")
          @printMessage("Opponent move is: #{data.msg}")
          App.board.move(data.msg)
          
        when "update_opponent_board"
          $("#messages").empty()
          if data.result
            color = "red"
            $('#opponentBoard').css 'pointerEvents', 'none'
            @printMessage("It's your opponent's turn!")
            hits_received.push data.id
            console.log(hits_received)
            if (game_over(hits_received))
              @printMessage("Your ships have been destroyed! Opponent wins!")
              alert "Your ships have been destroyed! Opponent wins!"
          else
            $('#opponentBoard').css 'pointerEvents', 'auto'
            color = "green"
            @printMessage("It's your turn!")

          console.log(data.result)
          $('#' + data.id, $('#myBoard')).css 'background-color', color
        when "update_my_board"
          $('#' + data.id, '#opponentBoard').css({'color': 'white', 'font-weight': 'bold', 'font-size': '16px'})
          $("#messages").empty()
          if data.result
            color = "red"
            $('#opponentBoard').css 'pointerEvents', 'auto'
            $('#' + data.id, '#opponentBoard').text('X')
            hits_done.push data.id
            console.log(hits_done)
            if (game_over(hits_done))
              @printMessage("Opponent ships destroyed! You win!")
              alert "Opponent ships destroyed! You win!"
              $('#opponentBoard').css 'pointerEvents', 'none'
            else
              @printMessage("It's your turn!")
          else
            $('#opponentBoard').css 'pointerEvents', 'none'
            $('#' + data.id, '#opponentBoard').text('0')
            color = "green"
            @printMessage("It's your opponent's turn!")

          console.log(data.result)
          $('#' + data.id, $('#opponentBoard')).css 'background-color', color

        when "opponent_forfeits"
          @printMessage("Opponent forfeits. You win!")

    printMessage: (message) ->
      $("#messages").append("<p>#{message}</p>")

    callFireShot: (id) ->
      @perform("fire_shot", id: id)

window.hits_received = []
window.hits_done = []

window.coordinatesToId = (i, j) ->
  return String.fromCharCode(65+i) + "-" + (j+1)

window.game_over = (hits) ->
  i = 0
  if hits.length == 17
    hits.sort()
    while i < 16
      if hits[i] == hits[i+1]
        return false
      i++
    return true
  else return false