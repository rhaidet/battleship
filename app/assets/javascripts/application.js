// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require welcomeScript.js
//= require turbolinks
//= require_tree .

$(document).ready(function () {
  window.onpageshow = function(event) {
  if (event.persisted) {
      window.location.reload()
    }
  };

  load_javascript = function(controller, action) {
    var controller_trigger_string = controller + ".load";
    var action_trigger_string     = controller + "_" + action + ".load";

    console.log("loading:"+controller_trigger_string);
    console.log("loading:"+action_trigger_string);

    $.event.trigger(controller_trigger_string);
    $.event.trigger(action_trigger_string);
  };

  var $selector  = $(".main_content");
  var controller = $selector.data('controller');
  var action     = $selector.data('action');

  load_javascript(controller, action);
});