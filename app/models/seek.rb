class Seek
  def self.create(uuid)
    if opponent = REDIS.spop("seeks")
    
      REDIS.set(opponent, Marshal.dump(Board.new.grid))
      REDIS.set(uuid, Marshal.dump(Board.new.grid))
      Game.start(uuid, opponent)
    else
      REDIS.sadd("seeks", uuid)
    end
  end

  def self.remove(uuid)
    REDIS.srem("seeks", uuid)
  end

  def self.clear_all
    REDIS.del("seeks")
  end
end