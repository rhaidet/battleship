class Game < ApplicationRecord
  has_many :players

  def self.start(uuid1, uuid2)
    ruby, java = [uuid1, uuid2].shuffle
    starting_player = [java, ruby].shuffle.sample


    ruby_map = Marshal.load(REDIS.get(ruby))
    java_map = Marshal.load(REDIS.get(java))

    if starting_player == ruby
      ActionCable.server.broadcast "player_#{ruby}", {action: "game_start", msg: "ruby", turn: true, map: ruby_map}
      ActionCable.server.broadcast "player_#{java}", {action: "game_start", msg: "java", turn: false, map: java_map}

    else
      ActionCable.server.broadcast "player_#{ruby}", {action: "game_start", msg: "ruby", turn: false, map: ruby_map}
      ActionCable.server.broadcast "player_#{java}", {action: "game_start", msg: "java", turn: true, map: java_map}

    end

    REDIS.set("opponent_for:#{ruby}", java)
    REDIS.set("opponent_for:#{java}", ruby)
  end

  def self.make_move(uuid, data)
    opponent = opponent_for(uuid)
    move_string = "#{data["from"]}-#{data["to"]}"

    ActionCable.server.broadcast "player_#{opponent}", {action: "make_move", msg: move_string}
  end

  def self.fire_shot(uuid, data)
    opponent = opponent_for(uuid)
    # map = Marshal.load(REDIS.get(opponent))
    x = data["id"].split('-')
    row = x[0].ord - 65
    col = x[1].to_i - 1
    map = Marshal.load(REDIS.get(opponent))
    result = map[row][col] != 0
    ActionCable.server.broadcast "player_#{opponent}", {action: "update_opponent_board", id: data["id"], result: result}
    ActionCable.server.broadcast "player_#{uuid}", {action: "update_my_board", id: data["id"], result: result}
  end

  def self.opponent_for(uuid)
    REDIS.get("opponent_for:#{uuid}")
  end

  def self.forfeit(uuid)
    if winner = opponent_for(uuid)
      ActionCable.server.broadcast "player_#{winner}", {action: "opponent_forfeits"}
    end
  end
end
