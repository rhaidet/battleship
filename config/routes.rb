Rails.application.routes.draw do
  get 'board/index'
  get 'board/generate'

  get 'player/index'
  get 'board/welcome'

  root 'board#welcome'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  mount ActionCable.server => "/cable"
end
